#! /usr/bin/python
'''
Copyright (C) 2005 Aaron Spike, aaron@ekips.org
              2015 Vasco Tenner

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

import sys
sys.path.append('/usr/share/inkscape/extensions')
import inkex
import gettext
_ = gettext.gettext

class MyEffect(inkex.Effect):
    path = '//svg:use'
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("-s", "--selectedonly",
            action="store", type="inkbool", 
            dest="selectedonly", default=False,
            help="Unlink only clones in selection")

    def effect(self):
        #first create path for extracted images
        svg = self.document.getroot()

        if (self.options.selectedonly):
            self.uncloneSelected(self.document, self.selected)
        else:
            self.uncloneAll(self.document)

    def uncloneSelected(self, document, selected):
        self.document=document #not that nice... oh well
        self.selected=selected
        if (self.options.ids):
            for id, select in selected.iteritems():
                for node in select.xpath(self.path, namespaces=inkex.NSS):
                    self.unlinkClone(node)

    def uncloneAll(self, document):
        self.document=document #not that nice... oh well
        # select al clones
        for node in self.document.getroot().xpath(self.path, namespaces=inkex.NSS):
            self.unlinkClone(node)

    def unlinkClone(self, node):
        # exbed the first embedded image
        xlink = node.get(inkex.addNS('href','xlink'))[1:] # remove #
        pattern = '//svg:*[@id=\'{}\']'.format(xlink)
        father = self.document.getroot().xpath(pattern,
                namespaces=inkex.NSS)[0] #assume ids to be unique
        id = node.attrib['id']
        node = father
        node.set('id', id)


if __name__ == '__main__':
    e = MyEffect()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 encoding=utf-8 textwidth=99
